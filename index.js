const express = require('express')
const app = express()
const port = 3001
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: false}));

let players = [];

let currentQuestion = 0;
let currentQuestionPoint = 10;

let acceptResponses = false;

let questions = [
    {answer: 'E'},
    {answer: 'A'},
    {answer: 'A'},
    {answer: 'A'},
    {answer: 'B'},
    {answer: 'C'},
    {answer: 'E'},
    {answer: 'C'},
    {answer: 'B'}
];

let question7Responses = [0, 0, 0, 0, 0];

const MessagingResponse = require('twilio').twiml.MessagingResponse;

app.get('/', (request, response) => {
    response.send('Hello from Express!')
});

app.get('/getCurrentState', (req, res) => {
	res.send({
		players: players,
		currentQuestion: currentQuestion,
		currentQuestionPoint: currentQuestionPoint,
		acceptResponses: acceptResponses,
		question7Responses: question7Responses
	}).end();
});

app.get('/restartGame', (req, res) => {

    currentQuestion = 0;
    players = [];
    currentQuestionPoint = 10;
    question7Responses = [0, 0, 0, 0, 0];
    acceptResponses = false;

    res.send({
        gameRestarted: true
    }).end();
});

app.get('/toggleQuiz', (req, res) => {
    acceptResponses = !acceptResponses;

    res.send({
        questionCanBeAnswered: acceptResponses
    }).end();

});

app.get('/toggleNextQuestion', (req, res) => {

    if(!acceptResponses) {
        acceptResponses = true;
    }

    if(currentQuestion === 6) {
        for (let i = 0; i < players.length; i++) {
            if(players[i].question7Answer !== null) players[i].points += question7Responses[players[i].question7Answer];
        }
    }

    currentQuestion += 1;
    currentQuestionPoint = 10;
    for (let i = 0; i < players.length; i++) {
        players[i].answered = false;
    }
    res.send({
        question: currentQuestion
    }).end();
});

app.get('/resetQuestionNumber', (req, res) => {
    currentQuestion = 0;
    res.send({
        question: currentQuestion
    }).end();
});

app.get('/getLeaderboard', (req, res) => {
    res.send({
        board: players
    }).end();
});

app.get('/getLeader', (req, res) => {

    let currentLeader = players[0];

    for (let i = 0; i < players.length; i++) {
        if(players[i].points > currentLeader.points) {
            currentLeader = players[i];
        }
    }

    res.send({
        leader: currentLeader
    }).end();
});

app.post('/answerQuestion', (req, res) => {
    const twiml = new MessagingResponse();

    if (!acceptResponses) {
        twiml.message('The question is not currently open!');
        res.writeHead(200, {'Content-Type': 'text/xml'});
        res.end(twiml.toString());
        return;
    }

    let request = req.body;

    let from = request.From;
    let message = request.Body;

    let player = null;

    for (let i = 0; i < players.length; i++) {
        if (from === players[i].player) {
            player = players[i];
        }
    }

    let playerIndex1 = -1;

    if(player) {
        for (let i = 0; i < players.length; i++) {
            if (players[i].player === from) {
                playerIndex1 = i;
            }
        }
    }

    if ((message === questions[currentQuestion].answer || currentQuestion === 6)) {

        let choiceIndex = 0;

        if(currentQuestion === 6) {
            if(message === 'A') {
                question7Responses[0] += 1;
            } else if(message === 'B') {
                question7Responses[1] += 1;
                choiceIndex = 1;
            } else if(message === 'C') {
                question7Responses[2] += 1;
                choiceIndex = 2;
            } else if(message === 'D') {
                question7Responses[3] += 1;
                choiceIndex = 3;
            } else if(message === 'E') {
                question7Responses[4] += 1;
                choiceIndex = 4;
            } else {
                twiml.message('Typo or something?');
                res.writeHead(200, { 'Content-Type': 'text/xml' });
                res.end(twiml.toString());
                return;
            }
        }

        if (!player) {
            players.push({
                player: from,
                points: currentQuestion === 6 ? 0 : currentQuestionPoint,
                currentQuestion: parseFloat(currentQuestion) + 1,
                answered: true,
                question7Answer: currentQuestion === 6 ? choiceIndex : null
            });
            currentQuestionPoint -= 1;
            if (currentQuestionPoint < 1) currentQuestionPoint = 1;

            twiml.message('Your response has been received.');
            res.writeHead(200, { 'Content-Type': 'text/xml' });
            res.end(twiml.toString());
            return;
        }
        else if(!player.answered) {
            let playerIndex = -1;
            for (let i = 0; i < players.length; i++) {
                if (players[i].player === from) {
                    playerIndex = i;
                }
            }
            if (playerIndex >= 0) {

                // if(players[playerIndex].currentQuestion !== currentQuestion) {
                //     twiml.message('Youve already answered this question!');
                //     res.writeHead(200, { 'Content-Type': 'text/xml' });
                //     res.end(twiml.toString());
                //     return;
                // }

                players[playerIndex].points = currentQuestion === 6 ? player.points : player.points + currentQuestionPoint;
                players[playerIndex].answered = true;
                players[playerIndex].question7Answer = currentQuestion === 6 ? choiceIndex : null;
                currentQuestionPoint -= 1;
                players[playerIndex].currentQuestion = players[playerIndex].currentQuestion + 1;
                if (currentQuestionPoint < 1) currentQuestionPoint = 1;

                twiml.message('Your response has been received.');
                res.writeHead(200, { 'Content-Type': 'text/xml' });
                res.end(twiml.toString());
                return;
            } else {
                twiml.message('Something went wrong!');
                res.writeHead(200, { 'Content-Type': 'text/xml' });
                res.end(twiml.toString());
                return;
            }
        } else {
            twiml.message('Youve already answered!');
            res.writeHead(200, { 'Content-Type': 'text/xml' });
            res.end(twiml.toString());
            return;
        }
    } else {

        twiml.message(players[playerIndex1].answered ? 'Youve already answered this question!' : 'Your response has been received.');

        if(player) {
            players[playerIndex1].answered = true;
        } else {
            players.push({
                player: from,
                points: 0,
                currentQuestion: parseFloat(currentQuestion) + 1,
                answered: true
            });
        }

        res.writeHead(200, { 'Content-Type': 'text/xml' });
        res.end(twiml.toString());
        return;
    }

    // res.writeHead(200, {'Content-Type': 'text/xml'});
    // res.end(twiml.toString());
});

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }

    console.log(`server is listening on ${port}`)
})
